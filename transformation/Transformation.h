//
// Created by Piotr Szczerbiński on 31/01/2022.
//

#ifndef MPGKPROJECT_TRANSFORMATION_H
#define MPGKPROJECT_TRANSFORMATION_H


#include "../matrix/Matrix.h"
#include <cmath>
#include <string>

class Transformation {

private:
    ///for dimensions = 3 it returns matrix 4x4
    Matrix getScalingMatrix(double *ratios, int dimensions) {
        double **data = static_cast<double **>(malloc((dimensions + 1) * sizeof(double *)));
        for (int i = 0; i < dimensions + 1; i++) {
            data[i] = static_cast<double *>(malloc(dimensions + 1 * sizeof(double)));
            for (int j = 0; j < dimensions + 1; j++) {
                data[i][j] = 0;
            }
            data[i][i] = ratios[i];
        }
        data[dimensions][dimensions] = 1.0;

        return Matrix(dimensions + 1, dimensions + 1, data);
    }

public:
    /// RX  0   0   0
    /// 0   RY  0   0
    /// 0   0   RZ  0
    /// 0   0   0   1
    Matrix getScalingMatrix(double xRatio, double yRatio, double zRatio);

    /// RX  0   0
    /// 0   RY  0
    /// 0   0   1
    Matrix getScalingMatrix(double xRatio, double yRatio);

    /// | cos -sin |
    /// | sin  cos |
    Matrix get2DimRotationMatrix(double theta);

    ///      | 1   0    0 |         | cos  0  sin |         | cos -sin  0 |
    /// Rx = | 0 cos -sin |    Ry = | 0    1    0 |    Rz = | sin  cos  0 |
    ///      | 0 sin  cos |         | -sin 0  cos |         |  0    0   1 |
    Matrix get3DimRotationMatrix(std::string axis, double theta);

    /// |X'|   |1 0 a|   |X|   |X+a|
    /// |Y'| = |0 1 b| x |Y| = |Y+b|
    /// |1 |   |0 0 1|   |1|   |1  |
    Matrix get2DimTranslationMatrix(double xTrans, double yTrans);

    /// |X'|   |1 0 0 a|   |X|   |X+a|
    /// |Y'| = |0 1 0 b| x |Y| = |Y+b|
    /// |Z'|   |0 0 1 c|   |Z|   |Z+c|
    /// |1 |   |0 0 0 1|   |1|   |1  |
    Matrix get3DimTranslationMatrix(double xTrans, double yTrans, double zTrans);

};


#endif //MPGKPROJECT_TRANSFORMATION_H
