//
// Created by Piotr Szczerbiński on 31/01/2022.
//

#include "Transformation.h"

/// RX  0   0   0
/// 0   RY  0   0
/// 0   0   RZ  0
/// 0   0   0   1
Matrix Transformation::getScalingMatrix(double xRatio, double yRatio, double zRatio) {
    int dimensions = 3;
    double *ratios = new double[dimensions]{xRatio, yRatio, zRatio};
    return getScalingMatrix(ratios, dimensions);
}

/// RX  0   0
/// 0   RY  0
/// 0   0   1
Matrix Transformation::getScalingMatrix(double xRatio, double yRatio) {
    int dimensions = 2;
    double *ratios = new double[dimensions]{xRatio, yRatio};
    return getScalingMatrix(ratios, dimensions);
}

/// | cos -sin |
/// | sin  cos |
Matrix Transformation::get2DimRotationMatrix(double theta) {
    double **data = reinterpret_cast<double **>(new double[2][2]{
            {cos(theta), (-1) * sin(theta)},
            {sin(theta), cos(theta)}
    });
    return Matrix(2, 2, data);
}

///      | 1   0    0 |         | cos  0  sin |         | cos -sin  0 |
/// Rx = | 0 cos -sin |    Ry = | 0    1    0 |    Rz = | sin  cos  0 |
///      | 0 sin  cos |         | -sin 0  cos |         |  0    0   1 |
Matrix Transformation::get3DimRotationMatrix(std::string axis, double theta) {
    double **data;
    if (axis == "x") {
        data = reinterpret_cast<double **>(new double[3][3]{
                {1, 0,          0},
                {0, cos(theta), (-1) * sin(theta)},
                {0, sin(theta), cos(theta)}
        });
    } else if (axis == "y") {
        data = reinterpret_cast<double **>(new double[3][3]{
                {cos(theta),        0, sin(theta)},
                {0,                 1, 0},
                {(-1) * sin(theta), 0, cos(theta)}
        });
    } else /*if(axis == "z")*/ {
        data = reinterpret_cast<double **>(new double[3][3]{
                {cos(theta), (-1) * sin(theta), 0},
                {sin(theta), cos(theta),        0},
                {0,          0,                 1}
        });
    }
    return Matrix(3, 3, data);
}

/// |X'|   |1 0 a|   |X|   |X+a|
/// |Y'| = |0 1 b| x |Y| = |Y+b|
/// |1 |   |0 0 1|   |1|   |1  |
Matrix Transformation::get2DimTranslationMatrix(double xTrans, double yTrans) {
    double **data = reinterpret_cast<double **>(new double[4][4]{
            {1, 0, xTrans},
            {0, 1, yTrans},
            {0, 0, 1}
    });
    return Matrix(3, 3, data);
}

/// |X'|   |1 0 0 a|   |X|   |X+a|
/// |Y'| = |0 1 0 b| x |Y| = |Y+b|
/// |Z'|   |0 0 1 c|   |Z|   |Z+c|
/// |1 |   |0 0 0 1|   |1|   |1  |
Matrix Transformation::get3DimTranslationMatrix(double xTrans, double yTrans, double zTrans) {
    double **data = reinterpret_cast<double **>(new double[4][4]{
            {1, 0, 0, xTrans},
            {0, 1, 0, yTrans},
            {0, 0, 1, zTrans},
            {0, 0, 0, 1}
    });
    return Matrix(4, 4, data);
}