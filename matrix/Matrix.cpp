//
// Created by Piotr Szczerbiński on 31/01/2022.
//

#include "Matrix.h"

Matrix::Matrix(int rows, int columns, double** data) {
    this->rows = rows;
    this->columns = columns;
    this->data = data;
}

double Matrix::getElement(int row, int column) {
    return data[row][column];
}

void Matrix::setElement(int row, int column, double value) {
    data[row][column] = value;
}

Matrix Matrix::createIdentityMatrix(int dimension) {
    double **data = static_cast<double **>(malloc(dimension * sizeof(double *)));
    for(int i = 0; i < dimension; i++) {
        data[i] = static_cast<double *>(malloc(dimension * sizeof(double)));
    }
    for(int i = 0; i < dimension; i++) {
        for(int j = 0; j < dimension; j++) {
            if(i == j) {
                data[i][j] = 1;
            } else {
                data[i][j] = 0;
            }
        }
    }
    return Matrix(dimension, dimension, data);
}

void Matrix::transposeMatrix() {
    int rowsNumber = this->rows;
    int columnsNumber = this->columns;
    double **newData = static_cast<double **>(malloc(columnsNumber * sizeof(double *)));
    for(int i = 0; i < columnsNumber; i++) {
        newData[i] = static_cast<double *>(malloc(rowsNumber * sizeof(double)));
    }

    for(int i = 0; i < columnsNumber; i++) {
        for(int j = 0; j < rowsNumber; j++) {
            newData[i][j] = this->data[j][i];
        }
    }

    this->data = newData;
    this->rows = columnsNumber;
    this->columns = rowsNumber;
}