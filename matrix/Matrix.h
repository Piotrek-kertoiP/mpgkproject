//
// Created by Piotr Szczerbiński on 31/01/2022.
//

#ifndef MPGKPROJECT_MATRIX_H
#define MPGKPROJECT_MATRIX_H


#include <vector>

class Matrix {

private:
    int rows, columns;
    double **data;

public:
    Matrix(int rows, int columns, double** data);

    double getElement(int row, int column);

    void setElement(int row, int column, double value);

    Matrix createIdentityMatrix(int dimension);

    void transposeMatrix();
};

#endif //MPGKPROJECT_MATRIX_H
