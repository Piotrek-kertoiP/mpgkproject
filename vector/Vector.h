//
// Created by Piotr Szczerbiński on 31/01/2022.
//

#ifndef MPGKPROJECT_VECTOR_H
#define MPGKPROJECT_VECTOR_H


#include <cstdlib>
#include <cstring>

class Vector {

private:
    int dimensions;
    double *members;

public:
    Vector(int newDimensions, double *newMembers);

    Vector();

    int getDimensions();

    double *getMembers();

    void setDimensions(int newLength);

    void setMembers(double *newMembers);

    double getLength();

    void normalize();

    double scalarProduct(Vector other);

    Vector vectorProduct(Vector other);

    Vector operator+ (const Vector& rhs);

};


#endif //MPGKPROJECT_VECTOR_H
