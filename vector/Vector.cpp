//
// Created by Piotr Szczerbiński on 31/01/2022.
//

#include "Vector.h"
#include <cmath>

Vector::Vector(int newDimensions, double newMembers[]) {
    dimensions = newDimensions;
    for (int i = 0; i < newDimensions; i++) {
        members[i] = newMembers[i];
    }
}

Vector::Vector() {
    dimensions = 3;
    members = new double[3]{0.0, 0.0, 0.0};
}


int Vector::getDimensions() {
    return dimensions;
}

double *Vector::getMembers() {
    return members;
}

void Vector::setDimensions(int newLength) {
    this->dimensions = newLength;
}

void Vector::setMembers(double *newMembers) {
    this->members = newMembers;
}

double Vector::getLength() {
    double vectorLengthSquare = 0.0;
    for (int i = 0; i < this->dimensions; i++) {
        vectorLengthSquare += pow(this->members[i], 2);
    }
    double vectorLength = sqrt(vectorLengthSquare);
    return vectorLength;
}

/// scale the vector so that vector's dimensions is equal 1
void Vector::normalize() {
    double vectorLength = this->getLength();

    for (int i = 0; i < this->dimensions; i++) {
        this->members[i] /= vectorLength;
    }
}

/// note: works only for happy path
/// Vector(3, {a0, a1, a2}).scalarProduct(Vector(3, {b0, b1, b2})) =
///     = a0*b0 + a1*b1 + a2*b2
double Vector::scalarProduct(Vector other) {
    double result = 0.0;
    for (int i = 0; i < this->dimensions; i++) {
        result += this->members[i] * other.getMembers()[i];
    }
    return result;
}

/// note: works only for 3-dim vectors
/// Vector(3, {a0, a1, a2}).vectorProduct(Vector(3, {b0, b1, b2})) =
///     = Vector(3, {a1*ab - a2*b1, a2*b0 - a0*b2, a0*b1 - a1*b0})
Vector Vector::vectorProduct(Vector other) {
    double x = this->members[1] * other.getMembers()[2] - this->members[2] * other.getMembers()[1];
    double y = this->members[2] * other.getMembers()[0] - this->members[0] * other.getMembers()[2];
    double z = this->members[0] * other.getMembers()[1] - this->members[1] * other.getMembers()[0];
    auto *newMembers = new double[3]{x, y, z};
    return Vector(3, newMembers);
}

Vector Vector::operator+ (const Vector& rhs){
    double x = this->members[0] + rhs.members[0];
    double y = this->members[0] + rhs.members[0];
    double z = this->members[0] + rhs.members[0];
    auto *newMembers = new double[3]{x, y, z};
    return Vector(3, newMembers);
}